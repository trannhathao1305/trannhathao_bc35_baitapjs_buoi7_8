var numberArray = [];
function themSo() {
  var n = document.getElementById("so-n").value * 1;
  numberArray.push(n);
  document.getElementById("showNumber").innerHTML = numberArray;
}

// Bài 1: Tổng số dương
function tinhTong() {
  var tong = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      tong += numberArray[i];
    }
  }
  document.getElementById("tong-so-n").innerHTML = tong;
}

// Bài 2: Đếm số dương
function demSoDuong() {
  var soDuong = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] > 0) {
      soDuong++;
    }
  }
  document.getElementById("dem-so-duong").innerHTML = soDuong;
}

// Bài 3: Tìm số nhỏ nhất
function soNhoNhat() {
  var duongNhoNhat = numberArray[0];
  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] < duongNhoNhat) {
      duongNhoNhat = numberArray[i];
    }
  }
  document.getElementById("so-nho-nhat").innerHTML = duongNhoNhat;
}

// Bài 4: Tìm số dương nhỏ nhất
function timSoDuongNhoNhat() {
  var array = [];

  for (var i = 0; i < numberArray.length; i++) {
    if (numberArray[i] >= 0) {
      array.push(numberArray[i]);
    } else {
      min = "Không có số dương trong mảng!";
    }
  }
  var min = array[0];
  for (var j = 0; j < array.length; j++) {
    if (array[j] < min) {
      min = array[j];
    }
  }
  document.getElementById("result4").innerHTML = `Số dương nhỏ nhất: ` + min;
}

// Bài 5: Tìm số chẵn cuối cùng
function btnTimSoChan() {
  var array = [];
  for (var i = 0; i < numberArray.length; i++) {
    array.push(numberArray[i]);
  }
  var soChanCuoi;
  for (var i = array.length - 1; i >= 0; i--) {
    if (array[i] % 2 == 0) {
      soChanCuoi = array[i];
      break;
    } else {
      soChanCuoi = -1;
    }
  }
  document.getElementById(
    "result5"
  ).innerHTML = `Số chẵn cuối cùng: ${soChanCuoi}`;
}

// Bài 6: Đổi chỗ
function btnDoiCho() {
  var array = [];
  for (var i = 0; i < numberArray.length; i++) {
    array.push(numberArray[i]);
  }
  for (var i = 0; i < array.length - 1; i++) {
    var viTri1 = Number(document.getElementById("vitri-1").value);
    var viTri2 = Number(document.getElementById("vitri-2").value);
    var temp = array[viTri1];
    array[viTri1] = array[viTri2];
    array[viTri2] = temp;
  }
  document.getElementById("result6").innerHTML = "Mảng sau khi đổi: " + array;
}

// Bài 7: Sắp xếp tăng dần
function btnSapXepTangDan() {
  var array = [];
  for (var i = 0; i < numberArray.length; i++) {
    array.push(numberArray[i]);
  }
  for (var i = 0; i < array.length - 1; i++) {
    for (var j = 0; j < array.length; j++) {
      if (array[j] > array[j + 1]) {
        let temp = array[j];
        array[j] = array[j + 1];
        array[j + 1] = temp;
      }
    }
  }
  document.getElementById("result7").innerHTML =
    "Mảng sau khi sắp xếp: " + array;
}

// Bài 8: Tìm số nguyên tố đầu tiên
var soNTDauTien;
function checkSNT(number) {
  if (number <= 1) {
    return 0;
  }
  for (let j = 2; j <= Math.sqrt(number); j++) {
    if (number % j == 0) {
      return 0;
    }
  }
  return soNTDauTien;
}
function btnSoNTDauTien() {
  for (let i = 0; i <= numberArray.length; i++) {
    if (checkSNT(numberArray[i])) {
      soNTDauTien = numberArray[i];
      break;
    } else {
      soNTDauTien = -1;
    }
  }
  document.getElementById("result8").innerHTML = soNTDauTien;
}

// Bài 9: Đếm số nguyên
function themSoN() {
  var n = Number(document.getElementById("nhap-so").value);
  numberArray.push(n);
  document.getElementById("result9a").innerHTML = numberArray;
}

function demSoNguyen() {
  var countSoNguyen = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (Number.isInteger(numberArray[i])) {
      countSoNguyen++;
    }
  }
  document.getElementById("result9b").innerHTML =
    "Số nguyên : " + countSoNguyen;
}

// Bài 10: So sánh số lượng số âm và số dương
function btnSoSanh() {
  var countDuong = 0;
  var countAm = 0;
  var content = "";
  for (var i = 0; i <= numberArray.length; i++) {
    if (numberArray[i] >= 0) {
      countDuong++;
    } else if (numberArray[i] < 0) {
      countAm++;
    }
  }
  if (countDuong > countAm) {
    content = "Số dương " + ">" + " Số âm";
  } else if (countDuong < countAm) {
    content = "Số âm " + ">" + " Số dương";
  } else {
    content = "Số dương " + "=" + " Số âm";
  }
  document.getElementById("result10").innerHTML = content;
}
